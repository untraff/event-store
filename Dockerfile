FROM ubuntu:20.04

RUN apt-get -yqq update
RUN apt-get -yqq install python3-pip python3-dev

ADD . /opt/src
WORKDIR /opt/src

RUN pip3 install --no-cache-dir -r requirements.txt

EXPOSE 8000

CMD ["python3", "./manage.py", "runserver"]