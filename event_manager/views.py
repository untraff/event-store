from rest_framework import viewsets, generics

from event_manager.models import Event
from event_manager.serializers import EventSerializer


class EventViewSet(generics.ListCreateAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
