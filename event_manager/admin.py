from core.admin import admin_site
from event_manager.models import Event

admin_site.register(Event)
