from django.db import models


class Event(models.Model):
    user_id = models.CharField(max_length=255)
    event_name = models.CharField(max_length=255)
    event_timestamp = models.IntegerField()

    def __str__(self):
        return f"{self.event_name} for user: {self.user_id}"
