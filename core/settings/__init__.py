import os

from .base import *

if os.getenv("PROD_MODE") == "1":
    from .prod import *
else:
    from .dev import *


try:
    from .local import *
except ImportError:
    pass
