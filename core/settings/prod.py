import os

DEBUG = False
SECRET_KEY = os.getenv(
    "DJANGO_SECRET_KEY", "wu04=_0edk*$j#=4pde!gu-f5u74#^s#ev)f!o3+kn)uh1rnw7"
)

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.getenv("DB_NAME", "events"),
        "USER": os.getenv("DB_USER", "admin"),
        "PASSWORD": os.getenv("DB_PASSWORD", "123"),
        "HOST": os.getenv("DB_HOST", "127.0.0.1"),
        "PORT": os.getenv("DB_PORT", "5432"),
    }
}
