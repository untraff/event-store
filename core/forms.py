from django import forms


class CreativeUploadForm(forms.Form):
    myfile = forms.FileField()
