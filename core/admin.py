from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.models import User, Group

from core.views import CreativeUploadView


class CustomAdminSite(admin.AdminSite):
    site_header = "Hello baby"

    def get_urls(self):
        urls = super(CustomAdminSite, self).get_urls()

        custom_urls = [
            url('creative-upload/', self.admin_view(CreativeUploadView.as_view()), name="creative-upload-url"),
        ]
        return urls + custom_urls


admin_site = CustomAdminSite(name='сadmin')
admin_site.register(User)
admin_site.register(Group)
