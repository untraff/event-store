import csv
import io

from django.urls import reverse_lazy
from django.views.generic import FormView

from core.forms import CreativeUploadForm


class CreativeUploadView(FormView):
    form_class = CreativeUploadForm
    template_name = "creative_upload.html"
    success_url = reverse_lazy('admin:creative-upload-url')
    
    def get_context_data(self, **kwargs):
        context = super(CreativeUploadView, self).get_context_data(**kwargs)
        context.update({
            "form": CreativeUploadForm(),
            "lol": "123"

        })
        return context

    def form_valid(self, form):
        csv_file = form.cleaned_data["myfile"]
        data_set = csv_file.read().decode('UTF-8')
        io_string = io.StringIO(data_set)
        next(io_string)
        for column in csv.reader(io_string, delimiter=',', quotechar='"'):
            campaign_id = column[0]
            push_id = column[1]
            hypo_id = column[2]
            title = column[3]
            body = column[4]
            link = column[5]
            language = column[6]
            icon_id = column[7]
            banner_id = column[8]
            print("magic")
        return super().form_valid(form)
