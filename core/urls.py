from django.conf.urls.static import static
from django.urls import path

from core import settings
from core.admin import admin_site
from event_manager.views import EventViewSet

urlpatterns = [
    path("events/", EventViewSet.as_view(), name="events"),
    path("", EventViewSet.as_view(), name="events"),
    path('admin/', admin_site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
