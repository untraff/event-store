## Dependencies

To install dependencies run

```pip install -r requirements.txt```

## Run application

To run the application type:

```python manage.py runserver```